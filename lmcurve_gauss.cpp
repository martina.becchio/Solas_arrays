/*
 * Library:   lmfit (Levenberg-Marquardt least squares fitting)
 *
 * File:      lmcurve.c
 *
 * Contents:  Implements lmcurve, a simplified API for curve fitting
 *            using the generic Levenberg-Marquardt routine lmmin.
 *
 * Copyright: Joachim Wuttke, Forschungszentrum Juelich GmbH (2004-2013)
 *
 * License:   see ../COPYING (FreeBSD)
 *
 * Homepage:  https://jugit.fz-juelich.de/mlz/lmfit
 *
 * Note to programmers: Don't patch and fork, but copy and modify!
 *   If you need to compute residues differently, then please do not patch
 * lmcurve.h and lmcurve.c, but copy them, and create differently named
 * versions of lmcurve_data_struct, lmcurve_evaluate, and lmcurve of your own.
 */

#include "lmcurve_gauss.h"
#include "lmmin_gauss.h"
#include <cmath>




void lmcurve_evaluate_gauss(
    const double* const par, const int m_dat, lmcurve_data_struct data,
    double* const fvec, int* const info, double t_data[], double y_data[],double (*const g)(const double t, const double *const par), int fitting)
{
    double showfvec, insp, gcalc, tcalc;
    for (int i = 0; i < m_dat; i++)
    {
        
        gcalc = gaussFitting(t_data[i],par);
        fvec[i] = y_data[i] - gcalc;//provare a passare g,t e y anziche lo struct in modo da chiamarle separatamente
        showfvec = fvec[i];
    }
}


void lmcurve_gauss(
    const int n_par, double *const par, const int m_dat,
    double t[], double y[], const double* const y_struct, const double* const t_struct,
    double (*const g)(const double t, const double *const par),
    const lm_control_struct *const control, lm_status_struct *const status, int fitting)
{
    
    lmcurve_data_struct data = {t_struct, y_struct, g};

    lmmin_gauss(n_par, par, m_dat, NULL, data,
          lmcurve_evaluate_gauss, control, status, t, y, g, fitting);
}

double gaussFitting(const double x, const double *params) // OKAY
{
    double y = 0;
    double cntr, amp, width;
    cntr = params[0];
    amp = params[1];
    width = params[2];
    y += (amp * exp(-0.5 * (pow((x - cntr) / width, 2))));
    return y;
}

