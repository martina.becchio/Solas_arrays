//calibration
/****************************************************************************
**
** Copyright (C) 2019 Stryker Ireland
**
** \brief Class for spectrometer calibration
** \file calibration.h
****************************************************************************/
#ifndef CALIBRATION_H
#define CALIBRATION_H

#include <stdio.h>
#include <initializer_list>
#include <iostream>
#include <random>
#include <cmath>
#include <vector>
#include <iostream>
#include <cmath>
using namespace std;

class calibration
{
    public:
        void startCalibration();
        void cancelCalibration();
        void endCalibration();
        void processCalibrationData();
        vector<double> readCalibrationFile();

    public:
        int specCount;
        vector<double> row;
        string line, word;
        string coeff, colname;
        vector<double> calData;
        vector<double> accumSpec;
        vector<double> averageSpec;
        vector<double> normSpec;
        vector<double> invSpec;
        vector<double> calVals = vector<double>(400);
};

#endif // CALIBRATION_H