/*
 * Library:   lmfit (Levenberg-Marquardt least squares fitting)
 *
 * File:      lmmin.h
 *
 * Contents:  Declarations for Levenberg-Marquardt minimization.
 *
 * Copyright: Joachim Wuttke, Forschungszentrum Juelich GmbH (2004-2018)
 *
 * License:   see ../COPYING (FreeBSD)
 *
 * Homepage:  https://jugit.fz-juelich.de/mlz/lmfit
 */

#ifndef LMMIN_H
#define LMMIN_H

#include "lmstruct.h"

__BEGIN_DECLS
typedef struct {
    const double *const t;
    const double *const y;
    double (*const g) (const double t, const double *par);
} lmcurve_data_struct;

/* Levenberg-Marquardt minimization. */
void lmmin2_gauss(
    double *const par, double *const parerr,
    double *const covar, const double *const y,
    lmcurve_data_struct data,
    void (*const evaluate)(
        const double *const par, const int m_dat, lmcurve_data_struct data,
        double *const fvec, int *const userbreak, double t_data[], double y_data[],double (*const g)(const double t, const double *const par), int fitting),
    const lm_control_struct *const control, lm_status_struct *const status, double t_data[], double y_data[],double (*const g)(const double t, const double *const par), int fitting);
/*
 *   This routine contains the core algorithm of our library.
 *
 *   It minimizes the sum of the squares of m nonlinear functions
 *   in n variables by a modified Levenberg-Marquardt algorithm.
 *   The function evaluation is done by the user-provided routine 'evaluate'.
 *   The Jacobian is then calculated by a forward-difference approximation.
 *
 *   Parameters:
 *
 *      n_par is the number of variables (INPUT, positive integer).
 *
 *      par is the solution vector (INPUT/OUTPUT, array of length n).
 *        On input it must be set to an estimated solution.
 *        On output it yields the final estimate of the solution.
 *
 *      parerr (either NULL or OUTPUT array of length n)
 *        yields parameter error estimates.
 *
 *      covar (either NULL or OUTPUT array of length n*n)
 *        yields the parameter covariance matrix (not yet implemented).
 *
 *      m_dat is the number of functions to be minimized (INPUT, integer).
 *        It must fulfill m>=n.
 *
 *      y contains data to be fitted. Use a null pointer if there are no data.
 *
 *      data is a pointer that is ignored by lmmin; it is however forwarded
 *        to the user-supplied functions evaluate and printout.
 *        In a typical application, it contains experimental data to be fitted.
 *
 *      evaluate is a user-supplied function that calculates the m functions.
 *        Parameters:
 *          n, x, m, data as above.
 *          fvec is an array of length m; on OUTPUT, it must contain the
 *            m function values for the parameter vector x.
 *          userbreak is an integer pointer. When *userbreak is set to a
 *            nonzero value, lmmin will terminate.
 *
 *      control contains INPUT variables that control the fit algorithm,
 *        as declared and explained in lmstruct.h
 *
 *      status contains OUTPUT variables that inform about the fit result,
 *        as declared and explained in lmstruct.h
 */

/* old, simpler interface, preserved for API compatibility */
void lmmin_gauss(
    const int n_par, double *const par, const int m_dat, const double *const y,
    lmcurve_data_struct data,
    void (*const evaluate)(
        const double *const par, const int m_dat, lmcurve_data_struct data,
        double *const fvec, int *const userbreak,double t_data[], double y_data[],double (*const g)(const double t, const double *const par), int fitting),
    const lm_control_struct *const control, lm_status_struct *const status, double t_data[], double y_data[],double (*const g)(const double t, const double *const par),int fitting);

/* Refined calculation of Eucledian norm. */
double lm_enorm_gauss(const int, const double *const);
double lm_fnorm_gauss(const int, const double *const, const double *const);

/* Internal, exported for test/run_qr */
void lm_qrfac_gauss(
    const int m, const int n, double *const A, int *const Pivot,
    double *const Rdiag, double *const Acnorm, double *const W);
const int ws_size_poly = (2*6+5*2+6*2+3*2*2)*sizeof(double) + 2*sizeof(int);
const int ws_size_gauss = (2*31+5*3+31*3+3*3*3)*sizeof(double) + 3*sizeof(int);
const int poly_points = 6;
const int poly_param_number = 2;
const int gauss_param_number = 3;
const int len = 31;
extern char ws_poly[ws_size_poly];
extern char ws_gauss[ws_size_gauss];
__END_DECLS
#endif /* LMMIN_H */
