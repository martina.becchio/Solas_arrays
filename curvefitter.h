//curvefitting
/****************************************************************************
**
** Copyright (C) 2019 Stryker Ireland
**
** \brief Class for fitting Gaussian curves etc to data.
** \file curvefitter.h
****************************************************************************/
#ifndef CURVEFITTER_H
#define CURVEFITTER_H

#include <stdio.h>
#include <initializer_list>
#include <iostream>
#include <random>
#include <cmath>
#include <vector>
//#include <QObject>
//#include <QtCharts/QAbstractSeries>
//#include <QtCharts/QXYSeries>
#include <iostream>
#include <cmath>

//#include <QVector>
//#include <QCoreApplication> //to keep gui responsive
using namespace std;
//QT_BEGIN_NAMESPACE
//class QQuickView; //this is for the Qt user interface
//QT_END_NAMESPACE
//QT_CHARTS_USE_NAMESPACE


extern int g_polyorder;
extern int g_gaussNumGaussians;
// SPI Comms
#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

// Struct to hold Gaussian peak results values for convenience
struct gaussPeaks{
    double peakCentre = 0;
    double peakAmplitude = 0;
    double peakWidth = 0;
};


    //explicit curveFitter(int gaussNumGaussians=1, int polyOrder=2, int minWavelengthIndex=122, int maxWavelengthIndex=187);
    //explicit curveFitter(int polyOrder=1, int minWavelengthIndex=105, int maxWavelengthIndex=231);

//signals:
    //void m_polyBaselineFiChanged();
    //void m_gaussianFluorophoreFitChanged();

    // Main fitting access functions


    //int m_fluorophoreType;  // 0=PPIX, 1=ICG

    int saturation = 0;
    int polyOrder = 1;
    int num_pixels=288;
    const int poly_points = 6;
    const int poly_param_number = 2;
    const int gauss_param_number = 3;
    int m_polyNumIterations = 30;  // Might change this going forward if required
    int m_gaussNumIterations = 30; // Might change this going forward if required
    const int m_wavelengthFitMin = 100; // The lowest wavelength index to use in the fit   122       (105 in python code)
    int m_wavelengthFitMax = 190; //190
    const int minWavelengthIndex = 100;//100; // The lowest wavelength index to use in the fit   122       (105 in python code)
    const int maxWavelengthIndex = 190;//190; // The highest wavelength index to use in the fit   187       (105+126 in python code)
    const int m_numberOfWavelengthPoints = maxWavelengthIndex - minWavelengthIndex + 1;
    double m_polyParameters[poly_param_number]; //order of polynomial + 1
    double m_gaussianParameters[gauss_param_number];
    //vector<double> m_gaussianParameters = vector<double>(3);
    const int linefit1 = 18; //18
    int saturationDetection = 1;
    const int linefit2 = 48; //48
    int Glim1 = 0;
    int Glim2 = linefit2-linefit1;
    double m_wavelengthValues[m_numberOfWavelengthPoints];
    double m_rawCurveData[m_numberOfWavelengthPoints];
    //vector<double> m_wavelengthValues = vector<double>(m_numberOfWavelengthPoints);
    //vector<double> m_rawCurveData = vector<double>(m_numberOfWavelengthPoints);
    const int len = linefit2+1-linefit1;
    double xWindow[len];
    double yWindow[len];
    double xWindow_gauss[len];
    double yWindow_gauss[len];
    double xWindow_poly[poly_points];
    double yWindow_poly[poly_points];
    double xWindow_poly_right[3];
    double yWindow_poly_right[3];
    double xWindow_poly_left[3];
    double yWindow_poly_left[3];
    double m_polyBaselineFit[len];
    double m_rawMinusBaseline[len];
    double m_gaussianFluorophoreFit[len];
    /*vector<double> xWindow = vector<double> (len);
    vector<double> yWindow = vector<double> (len);
    vector<double> xWindow_gauss = vector<double> (len);
    vector<double> yWindow_gauss = vector<double> (len);
    vector<double> xWindow_poly = vector<double>(0);
    vector<double> yWindow_poly = vector<double>(0);
    vector<double> xWindow_poly_right = vector<double>(3);
    vector<double> yWindow_poly_right = vector<double>(3);
    vector<double> xWindow_poly_left = vector<double>(3);
    vector<double> yWindow_poly_left = vector<double>(3);
    vector<double> m_polyBaselineFit = vector<double> (len);
    vector<double> m_rawMinusBaseline = vector<double> (len);
    vector<double> m_gaussianFluorophoreFit = vector<double> (len);*/
    double m_fluorMeasurement = 0;
    double flMeasure = 0.0;
    double errors[3];
    double m_fluorPeak1FreqMin= 0;
    double m_fluorPeak1FreqMax = 0;
    double m_fluorPeak1WidthMin = 0;
    double m_fluorPeak1WidthMax = 0;
    //these will have to be taken from the specific spectrometer
    float hamaData_A0 = 0;
    float hamaData_B1 = 0;
    float hamaData_B2 = 0;
    float hamaData_B3 = 0;
    float hamaData_B4 = 0;
    float hamaData_B5 = 0;
    //string coeff;
    //double hamaData;
    double calcL = 0;
    double calcM = 0;
    double calcH = 0;
    double errorL = 0;
    double errorM = 0;
	double errorH = 0;
	double maxErr = 0;

    // TEST FUNCTIONALITY

    int f_testCurveFitter();
    int m_testNumFitsPerformed = 0;
    void startCalibration();
    void cancelCalibration();
    void endCalibration();
    void processCalibrationData();
    int specCount = 0;
    //string line, word;
    //string colname;

    // Gets
    int get_numberOfDataPoints(int* startIndex=nullptr, int* endIndex=nullptr);
    //int get_fluorophoreType(){return m_fluorophoreType;}

    // Sets
    int set_rawData(int dataPoints[288], int arraySize);
    //void set_polyBaselineOrder(int order);
    //void set_gaussNumberOfGaussians(int numGaussians);
    void set_fluorophoreType(int typeVal);
    void set_fluorescenceMeausurementCalculationParameters(double peak1Freq, double peak1Width);
    double norm_pdf(double x, double mu, double sigma, double amp);
    double f_calculateFlMeasurement();
    //vector<float> readHamaData();
    // Results Data for Access Externally
    gaussPeaks m_gaussPeakResults;

    static double f_gaussianMixtureEval(const double x, const double* params); // This is static so that a reference to it can be passed to the minimisation function
    static double f_polynomialEval(const double x, const double* params);
    int f_setInitialParamGuesses(bool poly=true, bool gauss=true);
    int f_wavelengthGenerateWavelengthArray();

    double f_fitPolynomial(double curvePointsX[], double curvePointsY[], double params[], double xWindow[], double yWindow[]);
    int f_fitGaussians(double curvePointsX[], double curvePointsY[], double params[]);
 //   vector<double> calData;
   // vector<double> accumSpec;
  //  vector<double> averageSpec;
   // vector<double> normSpec;
   // vector<double> invSpec;
   // vector<double> calVals;



#endif
