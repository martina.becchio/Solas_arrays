/****************************************************************************
**
** Copyright (C) 2019 Stryker Ireland
**
** \file curvefitter.cpp
** \brief Class for fitting Gaussian curves etc to data.
**
****************************************************************************/
#include "curvefitter.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cmath>
#include "lmcurve_poly.h"
#include "lmcurve_gauss.h"
#include "testdata.h"
#include <time.h>
#include <vector>
#include <algorithm>
#include <stdio.h>
#include <initializer_list>
#include <random>
#include <sstream>
//#include "lmfit.hpp"
#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif
using namespace std;

//int polyOrder = 1;
int g_polyorder = 1; // Order of the polynomial to set. Had to use global here as lmcurve needs a static function (which can only access static variables) which doesn't have room for the order variable in its parameters
//g_polyorder = polyOrder;
//double m_polyParameters[g_polyorder+1];


///
/// brief curveFitter::f_polynomialEval
/// Evaluate a polynomial function f(x) for an input x value.
/// The order is retrieved from the global \ref g_polyorder
/// param x The x value for which to calculate a f(x)
/// param params The polynomial parameters.
/// return The f(x) value.
///
double f_polynomialEval(const double x, const double *params) // OKAY
{
    double y = 0;
    for (int i = 0; i < g_polyorder + 1; i++) // order is the highest exponent...so 2nd order is a.x^2+b.x^1+c
    {
        y += params[i] * pow(x, g_polyorder - i);
    }
    return y;
}

///
/// brief curveFitter::f_gaussianMixtureEval
/// Calculate the output of a function made up of a mixture of Gaussians.
/// The number of Gaussians is retrieved in the global \ref g_gaussNumGaussians.
/// param x The x value for which to calculate an f(x)
/// param params The parameters of the Gaussians, each 3 values [a,b,c] where a=centre, b=amplitude, c=width
/// return The f(x) for the input x.
///
double f_gaussianMixtureEval(const double x, const double *params) // OKAY
{
    double y = 0;
    double cntr, amp, width;
    cntr = params[0];
    amp = params[1];
    width = params[2];
    y += (amp * exp(-0.5 * (pow((x - cntr) / width, 2))));
    return y;
}

///
/// brief curveFitter::set_rawData
/// param dataPoints
/// return
///
int set_rawData(int dataPoints[], int arraySize)
{

    //if (m_wavelengthFitMax > (arraySize - 1))
      //  cerr << "Not enough data points in the acquisition vector for the specified indices.";
    
    f_wavelengthGenerateWavelengthArray();
    if (sizeof(m_rawCurveData) != m_numberOfWavelengthPoints)
        cerr << "Failed to create raw data vector.";

    //printf("start ywindow");

    for (int i = 0; i < m_numberOfWavelengthPoints; ++i) // m_numberOfWavelengthPoints is equal to maxwavelength-minwavelength+1
    {
        m_rawCurveData[i] = dataPoints[i+m_wavelengthFitMin];
        //printf("%f\n", m_rawCurveData[i]);
//m_rawCurveData[i] = dataPoints[i];

    }


    //calVals = cal.readCalibrationFile(); // read calibration values

    // multiply by cal values
    /*for(int i=0;i<m_numberOfWavelengthPoints;++i)
    {
        m_rawCurveData[i] = m_rawCurveData[i]*calVals[i];
    }
*/
    // window between 16 and 32
    //printf("start ywindow");
    for (int i = 0; i < linefit2 + 1 - linefit1; ++i)
    {
        yWindow[i] = m_rawCurveData[i + linefit1]; // selecting between lineft1 and linefit2 and calibrating
        //printf("%f\n", yWindow[i]);

    }
    return 0;
}

///
/// brief curveFitter::f_wavelengthSetValues
/// Calculate the wavelength values for each wavelength index.
/// return 0 for success.
///
/*vector<float> readHamaData()
{
    string filename = "C:\\Users\\mbecchio\\OneDrive - Stryker\\Desktop\\R&TD\\SOLAS\\20I00668.csv";
    ifstream hama(filename);
    for (int i = 0; i < 7; ++i)
    {
        getline(hama, coeff, ',');
        if (i != 0)
            hamaData.push_back(stof(coeff));
    }
    return hamaData;
}*/

///
/// brief curveFitter::f_wavelengthGenerateWavelengthArray
/// Generate the array of wavelength values from the min index to the max.
/// return 0 for success.
///
int f_wavelengthGenerateWavelengthArray()
{
    //JUST IMPORTING X FROM TESTDATA.H FOR NOW
    //hamaData = readHamaData();
    hamaData_A0 = 304.7124009;
    hamaData_B1 = 2.701836842;
    hamaData_B2 = -0.001032929;
    hamaData_B3 = -9.29E-06;
    hamaData_B4 = 1.70E-08;
    hamaData_B5 = -6.94E-12;
    for (int i = 0; i < m_numberOfWavelengthPoints; i++) // m_numberOfWavelengthPoints is equal to maxwavelength-minwavelength
    {
        double inputIndex = m_wavelengthFitMin + i +1; //remove 88
        m_wavelengthValues[i] = (hamaData_A0) + (hamaData_B1 * (inputIndex + 1)) + (hamaData_B2 * pow((inputIndex + 1), 2)) + (hamaData_B3 * pow((inputIndex + 1), 3)) + (hamaData_B4 * pow((inputIndex + 1), 4)) + (hamaData_B5 * pow((inputIndex + 1), 5)); // this corresponds to xVals in python
    }
    /*for (int i = 0; i < m_numberOfWavelengthPoints; i++) // m_numberOfWavelengthPoints is equal to maxwavelength-minwavelength
    {
        m_wavelengthValues[i] = hamaTestDataX[i+m_wavelengthFitMin];
    }*/

    for (int i = 0; i < linefit2 + 1 - linefit1; i++)
    {
        xWindow[i] = m_wavelengthValues[linefit1 + i];
    }
    return 0;
}

///
/// brief curveFitter::curveFitter
/// Constructor
/// param parent The owner of this class (nullptr)
/// param gaussNumGaussians The number of Gaussians to fit to the data.
/// param polyorder The order (highest exponent) of the polynomial to fit for background subtraction.
/// param numberOfWavelengths Th enumber of wavelength values (the x-axis)
///




///
/// brief curveFitter::get_numberOfDataPoints
/// param startIndex
/// param endIndex
/// return
///
int get_numberOfDataPoints(int *startIndex, int *endIndex)
{
    if (nullptr != startIndex)
        *startIndex = m_wavelengthFitMin;
    if (nullptr != endIndex)
        *endIndex = m_wavelengthFitMax;
    return m_numberOfWavelengthPoints;
}

///
/// brief curveFitter::f_performFits
/// param baseline
/// param gaussian
/// return -1 for baseline fit fail. -2 for Gaussian fit fail.
///
double f_performFits(bool baseline, bool gaussian)
{
	//set_polyBaselineOrder(polyOrder);
    
	set_rawData(hamaTestDataY, num_pixels);
    if (!(baseline | gaussian))
    {
        cerr << "No fit type selected.";
        return -1;
    }
    // \TEST whether to do this or not before each fit. Gauss fit is faster with this present.
    //f_setInitialParamGuesses(true, true);

    if (sizeof(yWindow) != sizeof(xWindow))
        cerr << "X and Y vectors must be the same length for fit.";
    //********** BASELINE *********//
    if (true == baseline)
    {
        f_setInitialParamGuesses(true, true);

        // here I select three points at the beginning and three at the end of the fitting window and use them to fit the polynomial
        for(int i=0; i<3; i++)
        {
            xWindow_poly[i] = xWindow[i];
            yWindow_poly[i] = yWindow[i];
            xWindow_poly[poly_points-i-1] = xWindow[len-i-1];
            yWindow_poly[poly_points-i-1] = yWindow[len-i-1];
        }
        
        m_polyBaselineFit[len] = f_fitPolynomial(xWindow_poly, yWindow_poly, m_polyParameters, xWindow, yWindow);
        //********** RAW - BASELINE *********//
        // Calculate the raw data minus baseline data
        for (int i = 0; i < sizeof(yWindow); ++i)
        {
            m_rawMinusBaseline[i] = yWindow[i] - m_polyBaselineFit[i]; // getting weird numbers here because m_polybaseline fit has only 6 entries
        }
    }
    //********** GAUSSIANS *********//
    if (true == gaussian)
    {
        //f_setInitialParamGuesses(false, true);
        for (int i=0; i<Glim2+1; i++)
        {
            xWindow_gauss[i] = xWindow[i+Glim1];
            yWindow_gauss[i] = m_rawMinusBaseline[i+Glim1];
        }
        
        if (f_fitGaussians(xWindow_gauss, yWindow_gauss, m_gaussianParameters))
        {
            cerr << "Gaussian fit failed.";
            return -2;
        }
    }

    m_gaussPeakResults.peakCentre = m_gaussianParameters[0];
    m_gaussPeakResults.peakAmplitude = m_gaussianParameters[1];
    m_gaussPeakResults.peakWidth = m_gaussianParameters[2];
    cout << "New gaussian parameters: " << m_gaussPeakResults.peakCentre << " -- " << m_gaussPeakResults.peakAmplitude << " -- " << m_gaussPeakResults.peakWidth << endl;
    //********** FLUORESCENT MEASUREMENT *********//
    m_fluorMeasurement = f_calculateFlMeasurement();

    // ********** DEBUG INFO **********//
    /*++m_testNumFitsPerformed;
    if(m_testNumFitsPerformed%10==0)
    {
        qDebug() << "Average fit time -- Poly: " << m_testTotalProcTimeForPolyFits/static_cast<double>(CLOCKS_PER_SEC)*1000.0/static_cast<double>(m_testNumFitsPerformed)
                 << "ms Gauss: " << m_testTotalProcTimeForGaussFits/static_cast<double>(CLOCKS_PER_SEC)*1000.0/static_cast<double>(m_testNumFitsPerformed);
        m_testTotalProcTimeForPolyFits = 0;
        m_testTotalProcTimeForGaussFits = 0;
        m_testNumFitsPerformed = 0;
    }*/
    return m_fluorMeasurement;
}

///
/// brief curveFitter::f_calculateFlMeasurement
/// return The calculated fluorescence measurement.
///

double norm_pdf(double x, double mu, double sigma, double amp)
{
    return (amp * exp(-0.5 * (pow((x - mu) / sigma, 2))));
}

double f_calculateFlMeasurement()
{
    

    double amp = m_gaussPeakResults.peakAmplitude;
    double centre = m_gaussPeakResults.peakCentre;
    double width = m_gaussPeakResults.peakWidth;

    double test_pdf = norm_pdf(xWindow_gauss[5], centre, width, amp);
    double test_y = yWindow_gauss[5];
    double test_div = ((test_pdf - test_y) / amp);

    errors[0]=(abs((norm_pdf(xWindow_gauss[11], centre, width, amp) - yWindow_gauss[11]) / amp)); //change to 13 15 17
    errors[1]=(abs((norm_pdf(xWindow_gauss[13], centre, width, amp) - yWindow_gauss[13]) / amp));
    errors[2]=(abs((norm_pdf(xWindow_gauss[15], centre, width, amp) - yWindow_gauss[15]) / amp));

    maxErr = *max_element(errors, errors+3);

    if (maxErr < 0.2)
        flMeasure = m_gaussPeakResults.peakAmplitude;
    else
        flMeasure = 0;

    return flMeasure;
}

///
/// brief curveFitter::f_fitPolynomial
/// param curvePointsX
/// param curvePointsY
/// param params
/// return 0 for success.
///
double f_fitPolynomial(double curvePointsX[], double curvePointsY[], double params[], double xWindow[], double yWindow[])
{
    lm_control_struct control;
    lm_status_struct status;
    control.ftol = .001;
    control.scale_diag = 1;
    control.xtol = 0.001;
    control.gtol = .001;
    control.verbosity = 0;
    control.patience = m_polyNumIterations;
    control.epsilon = 0.002;
    control.stepbound = 100;
    control.n_maxpri = -1;
    control.m_maxpri = -1;
    double *optParams = params;
    double* fitX = xWindow;
    double* fitY = yWindow;
    double *pointCurveX = curvePointsX;
    double *pointCurveY = curvePointsY;
    //size_t params_size = sizeof(params)/sizeof(params[0]);
    //size_t x_size = sizeof(curvePointsX)/sizeof(curvePointsX[0]);
    // Get the optimum poly params
    int fitting = 1;
    lmcurve_poly(poly_param_number,optParams,poly_points,curvePointsX,pointCurveY,curvePointsX,pointCurveY,f_polynomialEval,&control,&status, fitting);    // Evaluate the polynomial for each value

    for (int i = 0; i < sizeof(m_polyBaselineFit); ++i)
    {
        m_polyBaselineFit[i] = f_polynomialEval(fitX[i], optParams);
        if (m_polyBaselineFit[i] > fitY[i])
            m_polyBaselineFit[i] = fitY[i];
    }

    cout << "New poly parameters: " << optParams[0] << "--" << optParams[1] << endl;
    return m_polyBaselineFit[len];
}

///
/// brief curveFitter::f_fitGaussians
/// param curvePointsX
/// param curvePointsY
/// param params
/// return
///
int f_fitGaussians(double curvePointsX[], double curvePointsY[], double params[])
{
    lm_control_struct control;
    lm_status_struct status;
    control.ftol = .001;
    control.scale_diag = 1;
    control.xtol = 0.001;
    control.gtol = .001;
    control.verbosity = 0;
    control.patience = m_gaussNumIterations;
    control.epsilon = 0.002;
    control.stepbound = 100;
    control.n_maxpri = -1;
    control.m_maxpri = -1;
    double *optParams = params;
    double *curveX = curvePointsX;
    double *curveY = curvePointsY;
    //size_t g_params_size = sizeof(params)/sizeof(params[0]);
    //size_t g_x_size = sizeof(curvePointsX)/sizeof(curvePointsX[0]);
    int fitting = 2;
    lmcurve_gauss(gauss_param_number,optParams,len,curvePointsX,curvePointsY,curvePointsX,curvePointsY,f_gaussianMixtureEval,&control,&status,fitting);
    double p00 = optParams[0];
    double p10 = optParams[1];
    double p20 = optParams[2];

    for (int i = 0; i < len; ++i)
        m_gaussianFluorophoreFit[i] = f_gaussianMixtureEval(curveX[i], optParams);

    return 0;
}

int f_setInitialParamGuesses(bool poly, bool gauss)
{
    // set polynomial guess
    if (true == poly)
    {
        //for (int i = 0; i <= g_polyorder; i++)
        m_polyParameters[0] = 0;
        m_polyParameters[1] = 10000;// arbitrary between 1 and 100 (optimisation doesn't budge a zero if its in the initial data)
        cout << "Initial poly parameters: " << m_polyParameters[0] << " -- " << m_polyParameters[1] << endl;
    }

    // set Gaussian guess
    if (true == gauss)
    {
        m_gaussianParameters[0] = 635;
        m_gaussianParameters[1] = 1000;
        m_gaussianParameters[2] = 6;
        cout << "Initial gaussian parameters: " << m_gaussianParameters[0] << " -- " << m_gaussianParameters[1] << " -- " << m_gaussianParameters[2] << endl;
    }
    return 0;
}





int main()
{   int polyOrder = 1;
    //curveFitter curveFit;
    //curveFit.set_polyBaselineOrder(polyOrder);
    
    //curveFit.set_rawData(hamaTestDataY, hamaTestDataY.size());
    double m_fluorMeasurement = f_performFits(true,true);
    cout << "Fluorescence: "<< m_fluorMeasurement<<endl;
    
}


